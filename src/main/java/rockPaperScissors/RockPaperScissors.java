package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    
    /**
     * This is where the game loop happens. A new game round will be played for as long as "y" is the input.
     */    
    public void run() {
    	gameRound();
    	String input = readInput("Do you wish to continue playing? (y/n)?");
    	while(yesOrNo(input) == false) {
    		input = readInput("I do not understand " + input + ". Could you try again?");
    	}
    	if (input.equals("y")) {
    		roundCounter ++;
    		run();
    	}
    	else if (input.equals("n")) {
    		System.out.println("Bye bye :)");
    	}
    }
    
    /**
     * Plays a single round of the game
     */
    public void gameRound() {
    	System.out.println("Let's play round " + roundCounter);
    	String input = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
    	while(checkChoices(input) == false) {
    		input = readInput("I do not understand " + input + ". Could you try again?").toLowerCase();
    	}
    	System.out.println(checkWinner(input));
    	System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    }
    
    /**
     * Picks a random element form rpsChoiches list(this is the computer move)
     * @return string element of list
     */
    public String getRandomElement() {
        Random rand = new Random();
        return rpsChoices.get(rand.nextInt(rpsChoices.size()));
    }
    
    
    /**
     * This is where the logic of paper, scissor, rock is played out. It also increases the scores dependent on the winner
     * @param input
     * @return text that is dependent on the winner of a round
     */
    public String checkWinner(String input) {
        String computerInput = getRandomElement();
        String winText = "Human chose " + input + ", computer chose " + computerInput + ". Human wins!";
    	String tieText = "Human chose " + input + ", computer chose " + computerInput + ". It's a tie!";
    	String loseText = "Human chose " + input + ", computer chose " + computerInput + ". Computer wins!";
        if (input.equals(computerInput)) {
        	return tieText;
        }
        else if ((input.equals("rock") && computerInput.equals("scissors")) || (input.equals("scissors") && computerInput.equals("paper")) || (input.equals("paper") && computerInput.equals("rock")) ) {
        	humanScore ++;
        	return winText;
        }
        else {
        	computerScore ++;
        	return loseText;
        }	
    }
    
    /**
     * Checks if the input equals any of the elements in the rpsChoices list
     * @param input
     * @return true if input equals element in the list and false if not
     */
    public boolean checkChoices(String input) {
    	if (rpsChoices.contains(input)) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    /**
     * Checks if the input is either "y" or "n"
     * @param input
     * @return true if input is correct and false if not
     */
    public boolean yesOrNo(String input) {
    	if (input.equals("y") || input.equals("n")) {
    		return true;
    	}
    	else {
    		return false;
    	}
    }
    
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
